(function() {
  'use strict';

  angular
    .module('cplanding')
    .controller('MainController', MainController)
    .directive('startslider',function($timeout) {
    return {
       restrict: 'A',
       replace: true,
       template: '<ul class="bxslider">' +
                   '<li ng-repeat="picture in main.pictures">' +
                     '<img ng-src="{{picture.src}}" alt="" />' +
                   '</li>' +
                  '</ul>',
       link: function() {
         $timeout(function(){
           angular.element('.bxslider').bxSlider({
             mode: 'horizontal',
             auto: true,
             moveSlides: 2,
             autoControls: true,
             slideMargin: 0,
             slideWidth: 200,
             maxSlides: 10,
             minSlides: 2,
             pager: false
           });
         })
      }
    };
});

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr) {
    var vm = this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1471300053273;
    vm.showToastr = showToastr;
    vm.pictures = [];
    for (var i=1; i<=16;i++){
      vm.pictures.push({src: '/assets/images/custom' + i +'.png'});
    }

    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }
  }
})();
