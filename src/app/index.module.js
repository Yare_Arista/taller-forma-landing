(function() {
  'use strict';

  angular
    .module('cplanding', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'toastr']);

})();
